# car-parking-system

car-parking-system is a Java Spring Boot application that provides an API to retrieve the closest car parks to a given location along with their availability information. The application uses a PostgreSQL database to store static car park information and live updates on car park availability.

## Execution Flow And Assumptions

- A method which loads data from a csv file containing car parking details on application start.
- A scheduler method which run every 5 minutes for fetching real-time json response to get available lots for parking.
- An API endpoint to fetch the nearest car parking lot available for the provide latitude and longitude.
- Used Haversine formula to calculate distance between coordinates.
- Created a custom coordinate converter class to convert SVY21 to latitude and longitude.

## How It Works

- Static car parking information data is loaded from "https://beta.data.gov.sg/datasets/d_23f946fa557947f93a8043bbef41dd09/view".
- Real time car parking availability data is loaded from "https://api.data.gov.sg/v1/transport/carpark-availability".
- User provides latitude and longitude to an API endpoint which return nearest available parking slots.

## Installation and Setup

### Prerequisites

- Apache Maven
- Docker
- Docker Compose

### Running the Application

1. Clone the Repository: Clone this repository to your local machine using Git.

```bash
git clone git@gitlab.com:ttekglobalassigments/vanisha-car-parking-system.git
```
Or
```bash
git clone https://gitlab.com/ttekglobalassigments/vanisha-car-parking-system.git
```
2. Build the Application: Navigate to the project directory and build the application using Maven.

```bash
mvn clean install
```

3. Run Docker Containers: Run docker compose file to build and run all services.

```bash
docker-compose up --build
```
4. Get nearest car parks: The following api will return all the nearest car parks from your location

```bash
localhost:7000/carparks/nearest?latitude=1.37326&longitude=103.897&page=2&perPage=3
```

##### Note: Request parameter latitude and longitude are compulsory fields.

5. Shut down application: In order to shut down the application to need to run the following command in order to close all services
```bash
docker-compose down --volumes
```

## Project Structure

- /controller - Consists of all controller class and exception handler
- /converter - Consists of all the files related to the custom converter for SVY21 coordinates.
- /dto - Includes all POJO's
- /entity - Includes entity models
- /exception - Includes all custom exception classes
- /loader - Consists of a data loader classes to load data
- /repository - Includes model repository classes
- /scheduler - Includes scheduler classes
- /service - Consists of service classes with business logic
- /util - Includes all utils functions used throughout the application

## Future Improvements

- Implement caching to reduce the frequency of API calls for availability updates. 
- Add advanced error handling and logging for better debugging. 
- Enhance security measures, especially when dealing with external data sources.


