package com.ttek.assignment.controller;

import com.ttek.assignment.dto.AvailableCarParkDTO;
import com.ttek.assignment.service.CarParkService;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/carparks")
public class CarParkController {

  @Autowired
  private CarParkService carParkServiceImpl;

  @GetMapping("/nearest")
  public ResponseEntity<List<AvailableCarParkDTO>> getNearestCarParking(
      @RequestParam @NotNull double latitude, @RequestParam @NotNull double longitude,
      @RequestParam @Min(1) int pageNo, @RequestParam @Min(1) int perPage) throws Exception {
    List<AvailableCarParkDTO> carParkAvailable = carParkServiceImpl.getNearestCarParks(latitude,
        longitude, pageNo, perPage);
    return new ResponseEntity<>(carParkAvailable, HttpStatus.OK);
  }
}
