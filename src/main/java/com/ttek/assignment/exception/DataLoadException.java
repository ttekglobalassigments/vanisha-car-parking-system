package com.ttek.assignment.exception;

public class DataLoadException extends Exception {

  public DataLoadException(String message) {
    super(message);
  }
}
