package com.ttek.assignment.service;

import com.ttek.assignment.dto.AvailableCarParkDTO;
import com.ttek.assignment.entity.CarParkDetails;
import com.ttek.assignment.exception.DataLoadException;
import java.util.List;

public interface CarParkService {

  /**
   * Get the nearest car park slot available for the given latitude and longitude.
   * <p>
   * This method returns a list of ParkingLotAvailableResponse that contains five fields,
   * address,latitude,longitude, totalLots, availableLots
   *
   * This method is a shorthand for getting the nearest distance to the given longitude and
   * latitude.
   *
   * @param latitude  The latitude of the location
   * @param longitude The longitude of the location
   * @param pageNo    The page number for pagination
   * @param perPage   The number of items per page for pagination
   * @return A list of ParkingLotAvailableResponse with nearest car park information
   * @throws Exception if an error occurs during the process
   */

  List<AvailableCarParkDTO> getNearestCarParks(double latitude, double longitude, int pageNo,
      int perPage) throws Exception;

  /**
   * Saves a list of CarParkDetails to the database.
   *
   * This method utilizes the carParkDetailsRepository to save the provided list of CarParkDetails
   * to the database. If the save operation is successful, it logs a success message. If any
   * exception occurs during the save operation, it logs an error message and throws a
   * DataLoadException with the corresponding error message.
   *
   * @param carParkDetailsList List of CarParkDetails to be saved to the database.
   * @throws DataLoadException If an error occurs during the save operation.
   */
  void saveCarParkDetails(List<CarParkDetails> carParkDetailsList) throws DataLoadException;

}
