package com.ttek.assignment.service;

import com.ttek.assignment.dto.AvailableCarParkDTO;
import com.ttek.assignment.dto.CarParkDTO;
import com.ttek.assignment.entity.CarParkDetails;
import com.ttek.assignment.entity.CarParkStatus;
import com.ttek.assignment.exception.DataLoadException;
import com.ttek.assignment.repository.CarParkDetailsRepository;
import com.ttek.assignment.repository.CarParkStatusRepository;
import com.ttek.assignment.util.CoordinateDistanceCalculator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CarParkServiceImpl implements CarParkService {

  @Autowired
  private CarParkDetailsRepository carParkDetailsRepository;
  @Autowired
  private CarParkStatusRepository carParkStatusRepository;
  @Autowired
  private CoordinateDistanceCalculator coordinateDistanceCalculator;

  @Override
  public List<AvailableCarParkDTO> getNearestCarParks(double latitude, double longitude, int pageNo,
      int perPage) throws Exception {
    try {
      // Retrieve car park availabilities with lots available
      List<CarParkStatus> carParkAvailabilities = carParkStatusRepository.findByLotsAvailableGreaterThan(
          0);

      if (carParkAvailabilities == null) {
        log.info("No available lots!");
        return null;
      }

      // Calculate distances, map to DTOs, and convert to response
      List<CarParkDTO> carParkDTOs = calculateDistancesAndMapToDTOs(carParkAvailabilities, latitude,
          longitude);

      // Convert to paginated response
      List<AvailableCarParkDTO> response = convertToResponse(carParkDTOs);

      int start = (pageNo - 1) * perPage;
      int end = Math.min(start + perPage, response.size());
      return response.subList(start, end);
    } catch (Exception e) {
      log.error("Some Exception occurred while getting nearest car Parks: {}" + e.getMessage());
      throw new Exception("Some Exception occurred while getting nearest car Parks: {}");
    }
  }

  /**
   * Calculate distances between a location and available car parks, and map the results to
   * CarParkDTOs.
   *
   * @param carParkAvailabilities List of available car park statuses
   * @param latitude              The latitude of the location
   * @param longitude             The longitude of the location
   * @return List of CarParkDTOs with calculated distances
   * @throws Exception if an error occurs during the process
   */
  public List<CarParkDTO> calculateDistancesAndMapToDTOs(List<CarParkStatus> carParkAvailabilities,
      double latitude, double longitude) throws Exception {
    try {
      return carParkAvailabilities.stream().map(availability -> {
            try {
              // Retrieve parking lot details and calculate distance
              CarParkDetails carParkDetails = availability.getCarParkNo();
              double distance = coordinateDistanceCalculator.calculateDistance(latitude, longitude,
                  carParkDetails.getLatitude(), carParkDetails.getLongitude());

              // Create CarParkDTO
              return new CarParkDTO(carParkDetails, availability, distance);
            } catch (Exception e) {
              log.debug("Distance calculation returned an error: {}" + e.getMessage());
              return null;
            }
          }).filter(Objects::nonNull).sorted(Comparator.comparingDouble(CarParkDTO::getDistance))
          .collect(Collectors.toList());
    } catch (Exception e) {
      log.error("Some error occurred while sorting the response: {}" + e.getMessage());
      throw new Exception("Some error occurred while calculating distance.");
    }
  }


  /**
   * Convert a list of CarParkDTOs to a list of ParkingLotAvailableResponses.
   *
   * @param carParkDTO List of CarParkDTOs
   * @return List of ParkingLotAvailableResponses
   */
  private List<AvailableCarParkDTO> convertToResponse(List<CarParkDTO> carParkDTO) {
    List<AvailableCarParkDTO> responseList = new ArrayList<>();
    carParkDTO.forEach(carPark -> {
      AvailableCarParkDTO availableCarParkDTO = new AvailableCarParkDTO(carPark);
      responseList.add(availableCarParkDTO);
    });
    return responseList;
  }

  /**
   * Saves a list of CarParkDetails to the database.
   *
   * This method takes a List of CarParkDetails and uses the carParkDetailsRepository to save
   * the provided list to the database. If the save operation is successful, it logs an
   * information message indicating the success. If any exception occurs during the save
   * operation, it logs an error message, throws a DataLoadException, and includes the
   * exception message for better exception handling.
   *
   * @param carParkDetailsList List of CarParkDetails to be saved to the database.
   * @throws DataLoadException If an error occurs during the save operation, a DataLoadException
   *                           is thrown with the corresponding error message.
   */
  @Override
  public void saveCarParkDetails(List<CarParkDetails> carParkDetailsList) throws DataLoadException {
    try {
      carParkDetailsRepository.saveAll(carParkDetailsList);
      log.info("Car park data saved successfully!");
    } catch (Exception e) {
      log.error("Failed to save car park data to database {}" + e.getMessage());
      throw new DataLoadException(e.getMessage());
    }
  }
}
