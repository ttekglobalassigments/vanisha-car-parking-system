package com.ttek.assignment.repository;

import com.ttek.assignment.entity.CarParkDetails;
import com.ttek.assignment.entity.CarParkStatus;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarParkStatusRepository extends JpaRepository<CarParkStatus, Long> {

  CarParkStatus findByCarParkNo(CarParkDetails carParkNo);

  List<CarParkStatus> findByLotsAvailableGreaterThan(int lotsAvailable);
}
