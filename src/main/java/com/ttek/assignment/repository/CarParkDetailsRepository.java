package com.ttek.assignment.repository;

import com.ttek.assignment.entity.CarParkDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarParkDetailsRepository extends JpaRepository<CarParkDetails, String> {

}