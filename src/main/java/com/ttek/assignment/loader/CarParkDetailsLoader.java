package com.ttek.assignment.loader;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.ttek.assignment.converter.LatLonCoordinate;
import com.ttek.assignment.converter.SVY21Coordinate;
import com.ttek.assignment.entity.CarParkDetails;
import com.ttek.assignment.exception.DataLoadException;
import com.ttek.assignment.service.CarParkService;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CarParkDetailsLoader implements CommandLineRunner {

  @Autowired
  private CarParkService carParkService;

  // Utility method to convert SVY21 coordinates to LatLon coordinates
  private static LatLonCoordinate convertSVY21ToLatLon(double xCoord, double yCoord) {
    SVY21Coordinate svy21Coordinate = new SVY21Coordinate(xCoord, yCoord);
    return svy21Coordinate.asLatLon();
  }

  @Override
  public void run(String... args) {
    // Load data from CSV and save to database
    saveCarParkDetails();
  }

  /**
   * Saves Parking lot details from a csv file when the application loads.
   */
  private void saveCarParkDetails() {
    try (InputStream resourceStream = getClass().getResourceAsStream(
        "/HDBCarparkInformation.csv");
        CSVReader reader = new CSVReaderBuilder(
        new InputStreamReader(resourceStream)).withSkipLines(1).build()) {

      List<String[]> records = reader.readAll();
      List<CarParkDetails> carParkDetailsList = new ArrayList<>();

      try {
        for (String[] data : records) {
          CarParkDetails carPark = loadCarParkData(data);
          carParkDetailsList.add(carPark);
        }
      } catch (Exception e) {
        log.error("Error loading car park data: {}", e.getMessage());
        throw new DataLoadException(e.getMessage());
      }
      carParkService.saveCarParkDetails(carParkDetailsList);
      log.info("Car park loader completed its job!");

    } catch (CsvException e) {
      log.error("Error loading CSV file: {}" + e.getMessage());
    } catch (Exception e) {
      log.error("Error loading data: {}" + e.getMessage());
    }
  }

  /**
   * Set parking lot details as per the database model.
   *
   * This method returns an object of Parking lot details.
   *
   * @param data array of input streams from the API.
   * @return the Parking lot details.
   */
  public CarParkDetails loadCarParkData(String[] data) {
    CarParkDetails carPark = new CarParkDetails();
    carPark.setCarParkNo(data[0]);
    carPark.setAddress(data[1]);

    carPark.setXCoord(Double.parseDouble(data[2]));
    carPark.setYCoord(Double.parseDouble(data[3]));

    carPark.setCarParkType(data[4]);
    carPark.setTypeOfParkingSystem(data[5]);
    carPark.setShortTermParking(data[6]);
    carPark.setFreeParking(data[7]);
    carPark.setNightParking(data[8]);
    carPark.setCarParkDecks(data[9]);
    carPark.setGantryHeight(data[10]);
    carPark.setCarParkBasement(data[11]);

    // Coordinates conversion from x and y to latitude and longitude
    LatLonCoordinate convertedCoordinates = convertSVY21ToLatLon(Double.valueOf(data[2]),
        Double.valueOf(data[3]));
    carPark.setLatitude(convertedCoordinates.getLatitude());
    carPark.setLongitude(convertedCoordinates.getLongitude());
    return carPark;
  }
}
