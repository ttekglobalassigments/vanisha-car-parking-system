package com.ttek.assignment.converter;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LatLonCoordinate {
    private final double latitude;
    private final double longitude;

    public SVY21Coordinate asSVY21() {
        return SVY21.computeSVY21(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LatLonCoordinate other = (LatLonCoordinate) obj;
        if (Double.doubleToLongBits(latitude) != Double.doubleToLongBits(other.latitude)) {
            return false;
        }
        return Double.doubleToLongBits(longitude) == Double.doubleToLongBits(other.longitude);
    }
}
