package com.ttek.assignment.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "car_park_details")
public class CarParkDetails {

  @Id
  @Column(name = "car_park_no")
  private String carParkNo;

  @Column(name = "address")
  private String address;

  @Column(name = "x_coord")
  private double xCoord;

  @Column(name = "y_coord")
  private double yCoord;

  @Column(name = "car_park_type")
  private String carParkType;

  @Column(name = "type_of_parking_system")
  private String typeOfParkingSystem;

  @Column(name = "short_term_parking")
  private String shortTermParking;

  @Column(name = "free_parking")
  private String freeParking;

  @Column(name = "night_parking")
  private String nightParking;

  @Column(name = "car_park_decks")
  private String carParkDecks;

  @Column(name = "gantry_height")
  private String gantryHeight;

  @Column(name = "car_park_basement")
  private String carParkBasement;

  @Column(name = "latitude")
  private double latitude;

  @Column(name = "longitude")
  private double longitude;
}
