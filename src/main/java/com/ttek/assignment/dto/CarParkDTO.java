package com.ttek.assignment.dto;

import com.ttek.assignment.entity.CarParkDetails;
import com.ttek.assignment.entity.CarParkStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarParkDTO {

  private String address;
  private double latitude;
  private double longitude;
  private int totalLots;
  private int availableLots;
  private double distance;

  public CarParkDTO(CarParkDetails carParkDetails, CarParkStatus carParkStatus, double distance) {
    this.address = carParkDetails.getAddress();
    this.availableLots = carParkStatus.getLotsAvailable();
    this.latitude = carParkDetails.getLatitude();
    this.longitude = carParkDetails.getLongitude();
    this.totalLots = carParkStatus.getTotalLots();
    this.distance = distance;
  }
}
