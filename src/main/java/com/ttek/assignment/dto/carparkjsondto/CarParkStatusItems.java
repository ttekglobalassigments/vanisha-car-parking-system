package com.ttek.assignment.dto.carparkjsondto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
public class CarParkStatusItems {

  @JsonProperty("timestamp")
  private String timestamp;

  @JsonProperty("carpark_data")
  private List<CarParkItemData> carparkData;
}
