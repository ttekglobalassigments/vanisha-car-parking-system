package com.ttek.assignment.dto.carparkjsondto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
public class CarParkItemData {

  @JsonProperty("carpark_info")
  private List<CarParkDataInfo> carparkDataInfo;

  @JsonProperty("carpark_number")
  private String carparkNumber;

  @JsonProperty("update_datetime")
  private String updateDatetime;
}
