package com.ttek.assignment.dto.carparkjsondto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
public class CarParkStatus {

  @JsonProperty("items")
  private List<CarParkStatusItems> items;
}
