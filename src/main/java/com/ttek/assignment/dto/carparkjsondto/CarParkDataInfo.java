package com.ttek.assignment.dto.carparkjsondto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CarParkDataInfo {

  @JsonProperty("total_lots")
  private int totalLots;

  @JsonProperty("lots_available")
  private int availableLots;
}
