package com.ttek.assignment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AvailableCarParkDTO {

  private String address;
  private double latitude;
  private double longitude;
  private int totalLots;
  private int availableLots;

  public AvailableCarParkDTO(CarParkDTO carParkDTO) {
    this.address = carParkDTO.getAddress();
    this.availableLots = carParkDTO.getAvailableLots();
    this.latitude = carParkDTO.getLatitude();
    this.longitude = carParkDTO.getLongitude();
    this.totalLots = carParkDTO.getTotalLots();
  }
}
