package com.ttek.assignment.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CoordinateDistanceCalculator {

  // Earth radius in miles = 3,958.8 mi
  private static final double EARTH_RADIUS_KM = 6371.0;

  /**
   * Calculate the Haversine distance between two geographical coordinates.
   *
   * @param lat1 Latitude of the first point
   * @param lon1 Longitude of the first point
   * @param lat2 Latitude of the second point
   * @param lon2 Longitude of the second point
   * @return The calculated distance between the two points
   * @throws Exception if an error occurs during the process
   */
  public double calculateDistance(double lat1, double lon1, double lat2, double lon2)
      throws Exception {
    try {
      // Convert latitude and longitude from degrees to radians
      double radLat1 = Math.toRadians(lat1);
      double radLon1 = Math.toRadians(lon1);
      double radLat2 = Math.toRadians(lat2);
      double radLon2 = Math.toRadians(lon2);

      // Calculate differences
      double deltaLat = radLat2 - radLat1;
      double deltaLon = radLon2 - radLon1;

      // Haversine formula
      double a =
          Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(
              Math.sin(deltaLon / 2), 2);
      double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

      // Calculate distance
      double distance = EARTH_RADIUS_KM * c;
      return distance;
    } catch (Exception e) {
      log.error("Distance calculation error");
      throw new Exception("Distance calculation error");
    }
  }

}
