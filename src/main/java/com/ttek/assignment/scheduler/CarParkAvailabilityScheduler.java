package com.ttek.assignment.scheduler;

import com.ttek.assignment.entity.CarParkDetails;
import com.ttek.assignment.entity.CarParkStatus;
import com.ttek.assignment.repository.CarParkDetailsRepository;
import com.ttek.assignment.repository.CarParkStatusRepository;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class CarParkAvailabilityScheduler {

  @Value(value = "${car.park.availability.api}")
  private String CAR_PARK_AVAILABILITY_API;

  @Autowired
  private CarParkStatusRepository carParkStatusRepository;
  @Autowired
  private CarParkDetailsRepository carParkDetailsRepository;

  /**
   * Method to get real-time response from an API which is scheduled to run every 5th minute
   *
   * This method is a shorthand for the functionality for calculating total and available slots as
   * per car park no.
   */
  @Scheduled(cron = "${process.sync.cron.dateTime}")
  public void fetchAndSaveCarParkAvailability() throws Exception {
    try {
      log.info("Scheduler running for car park availability");
      // Fetch car park availability data from the API
      RestTemplate restTemplate = new RestTemplate();
      com.ttek.assignment.dto.carparkjsondto.CarParkStatus response = restTemplate.getForObject(
          CAR_PARK_AVAILABILITY_API, com.ttek.assignment.dto.carparkjsondto.CarParkStatus.class);

      if (response != null && response.getItems() != null) {
        Map<String, Integer> totalLotsMap = new HashMap<>();
        Map<String, Integer> availableLotsMap = new HashMap<>();

        // Extract total and available lots information from the API response
        response.getItems().forEach(item -> item.getCarparkData().forEach(carParkData -> {
          String carparkNumber = carParkData.getCarparkNumber();
          carParkData.getCarparkDataInfo().forEach(carParkInfo -> {
            int totalLots = carParkInfo.getTotalLots();
            int availableLots = carParkInfo.getAvailableLots();

            // Aggregate total and available lots for each car park
            totalLotsMap.merge(carparkNumber, totalLots, Integer::sum);
            availableLotsMap.merge(carparkNumber, availableLots, Integer::sum);
          });
        }));

        // Save the aggregated information to the database
        for (String carparkNumber : totalLotsMap.keySet()) {
          int totalLots = totalLotsMap.get(carparkNumber);
          int availableLots = availableLotsMap.get(carparkNumber);

          saveSlotInfoToDatabase(carparkNumber, totalLots, availableLots);
        }
        log.info("Scheduler has done its job!");
      }
    } catch (Exception e) {
      log.error("Scheduler was not able to complete its operations: {}" + e.getMessage());
      throw new Exception("Scheduler was not able to complete its operations");
    }
  }

  /**
   * Saves the available slots and total slots data into the database as per car park number.
   *
   * @param carparkNumber The car park number
   * @param totalLots     The total number of parking lots
   * @param availableLots The number of available parking lots
   * @throws Exception if an error occurs during the process
   */
  public void saveSlotInfoToDatabase(String carparkNumber, int totalLots, int availableLots)
      throws Exception {
    try {
      // Retrieve parking lot details based on car park number
      CarParkDetails carParkDetails = carParkDetailsRepository.findById(carparkNumber).orElse(null);
      if (carParkDetails != null) {
        // Check if CarParkStatus already exists for the given car park
        CarParkStatus carParkExists = carParkStatusRepository.findByCarParkNo(carParkDetails);
        if (carParkExists != null) {
          // Update existing ParkLotStatus with new availability information
          carParkExists.setLotsAvailable(availableLots);
          carParkExists.setTotalLots(totalLots);
          carParkStatusRepository.save(carParkExists);
        } else {
          // Create a new ParkLotStatus entry for the car park
          CarParkStatus carParkStatus = new CarParkStatus();
          carParkStatus.setLotsAvailable(availableLots);
          carParkStatus.setTotalLots(totalLots);
          carParkStatus.setCarParkNo(carParkDetails);
          carParkStatusRepository.save(carParkStatus);
        }
      }
    } catch (Exception e) {
      log.error("Failed to save to the database: {}" + e.getMessage());
      throw new Exception("Failed to save to the database: {}");
    }
  }
}
