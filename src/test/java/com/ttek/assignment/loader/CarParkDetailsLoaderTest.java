package com.ttek.assignment.loader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.ttek.assignment.entity.CarParkDetails;
import com.ttek.assignment.repository.CarParkDetailsRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CarParkDetailsLoaderTest {

  @Mock
  private CarParkDetailsRepository carParkDetailsRepository;

  @InjectMocks
  private CarParkDetailsLoader carParkDetailsLoader;

  @Test
  void testLoadCarParkData_SuccessfulConversion() {
    // Mock data
    String[] testData = {"CP001", "Test Address", "1.0", "2.0", "Type", "System", "Short", "Yes",
        "Night", "Decks", "Height", "Basement"};

    // Perform the method call
    CarParkDetails result = carParkDetailsLoader.loadCarParkData(testData);

    // Assertions
    assertNotNull(result);
    assertEquals("CP001", result.getCarParkNo());
    assertEquals("Test Address", result.getAddress());
    assertEquals(1.0, result.getXCoord());
    assertEquals(2.0, result.getYCoord());
    assertEquals("Type", result.getCarParkType());
    assertEquals("System", result.getTypeOfParkingSystem());
    assertEquals("Short", result.getShortTermParking());
    assertEquals("Yes", result.getFreeParking());
    assertEquals("Night", result.getNightParking());
    assertEquals("Decks", result.getCarParkDecks());
    assertEquals("Height", result.getGantryHeight());
    assertEquals("Basement", result.getCarParkBasement());
    assertNotNull(result.getLatitude());
    assertNotNull(result.getLongitude());
  }
}
