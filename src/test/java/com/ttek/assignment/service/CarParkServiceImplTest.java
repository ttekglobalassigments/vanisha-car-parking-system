package com.ttek.assignment.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import com.ttek.assignment.dto.AvailableCarParkDTO;
import com.ttek.assignment.dto.CarParkDTO;
import com.ttek.assignment.entity.CarParkDetails;
import com.ttek.assignment.entity.CarParkStatus;
import com.ttek.assignment.repository.CarParkDetailsRepository;
import com.ttek.assignment.repository.CarParkStatusRepository;
import com.ttek.assignment.util.CoordinateDistanceCalculator;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CarParkServiceImplTest {

  @Mock
  private CarParkDetailsRepository carParkDetailsRepository;
  @Mock
  private CarParkStatusRepository carParkStatusRepository;
  @Mock
  private CoordinateDistanceCalculator coordinateDistanceCalculator;
  @InjectMocks
  private CarParkServiceImpl carParkServiceImpl;

  @Test
  void testGetNearestCarParks_carParkingIsAvailable_availableCarParkingLots() throws Exception {
    double latitude = 10.0;
    double longitude = 20.0;
    int pageNo = 1;
    int perPage = 5;

    CarParkDetails carParkDetails = createDummyCarParkInfo("Test Address", "ABC1", 10.0,
        20.0);
    CarParkStatus carParkStatus = createDummyCarParkAvailability(carParkDetails, 50, 20);

    List<CarParkStatus> availabilityList = new ArrayList<>();
    availabilityList.add(carParkStatus);

    when(carParkStatusRepository.findByLotsAvailableGreaterThan(anyInt())).thenReturn(
        availabilityList);
    when(coordinateDistanceCalculator.calculateDistance(anyDouble(), anyDouble(), anyDouble(), anyDouble()))
        .thenReturn(10.0);

    List<AvailableCarParkDTO> result = carParkServiceImpl.getNearestCarParks(latitude,
        longitude, pageNo, perPage);

    assertEquals(1, result.size());
    assertEquals("Test Address", result.get(0).getAddress());
  }

  @Test
  void testGetNearestCarParks_carParkNotAvailable_returnNull() throws Exception {
    double latitude = 10.0;
    double longitude = 20.0;
    int pageNo = 1;
    int perPage = 5;

    CarParkDetails carParkDetails = createDummyCarParkInfo("Test Address", "ABC1", 10.0,
        20.0);
    CarParkStatus carParkStatus = createDummyCarParkAvailability(carParkDetails, 50, 20);

    List<CarParkStatus> availabilityList = new ArrayList<>();
    availabilityList.add(carParkStatus);

    when(carParkStatusRepository.findByLotsAvailableGreaterThan(anyInt())).thenReturn(null);
    when(carParkDetailsRepository.save(any(CarParkDetails.class))).thenReturn(null);

    List<AvailableCarParkDTO> result = carParkServiceImpl.getNearestCarParks(latitude,
        longitude, pageNo, perPage);

    assertNull(result);
  }

  @Test
  void testCalculateDistancesAndMapToDTOs_distanceCalculated_returnCarParkDtoList()
      throws Exception {
    CarParkDetails carParkDetails1 = createDummyCarParkInfo("Address1", "SDE1", 10.0, 20.0);
    CarParkStatus availability1 = createDummyCarParkAvailability(carParkDetails1, 50, 20);

    CarParkDetails carParkDetails2 = createDummyCarParkInfo("Address2", "HE2", 12.0, 22.0);
    CarParkStatus availability2 = createDummyCarParkAvailability(carParkDetails2, 30, 10);

    List<CarParkStatus> availabilityList = new ArrayList<>();
    availabilityList.add(availability1);
    availabilityList.add(availability2);

    when(coordinateDistanceCalculator.calculateDistance(anyDouble(), anyDouble(), anyDouble(), anyDouble()))
        .thenReturn(10.0);

    List<CarParkDTO> result = carParkServiceImpl.calculateDistancesAndMapToDTOs(availabilityList,
        10.0, 20.0);

    assertEquals(2, result.size());
    assertEquals("Address1", result.get(0).getAddress());
  }

  private CarParkDetails createDummyCarParkInfo(String address, String carParkNo, double xCoord,
      double yCoord) {
    CarParkDetails carParkDetails = new CarParkDetails();
    carParkDetails.setCarParkNo(carParkNo);
    carParkDetails.setAddress(address);
    carParkDetails.setXCoord(xCoord);
    carParkDetails.setYCoord(yCoord);
    carParkDetails.setCarParkType("Public");
    carParkDetails.setTypeOfParkingSystem("Automated");
    carParkDetails.setShortTermParking("Available");
    carParkDetails.setFreeParking("Yes");
    carParkDetails.setNightParking("Available");
    carParkDetails.setCarParkDecks("Multi-level");
    carParkDetails.setGantryHeight("2 meters");
    carParkDetails.setCarParkBasement("Yes");

    return carParkDetails;
  }

  private CarParkStatus createDummyCarParkAvailability(CarParkDetails carParkDetails,
      int totalLots, int availableLots) {
    CarParkStatus carParkStatus = new CarParkStatus();
    carParkStatus.setCarParkNo(carParkDetails);
    carParkStatus.setLotsAvailable(availableLots);
    carParkStatus.setTotalLots(totalLots);

    return carParkStatus;
  }
}
