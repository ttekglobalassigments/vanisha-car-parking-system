# For Java 17,
FROM openjdk:17-jdk-slim
WORKDIR /opt/app/assignment
COPY ./target/*.jar /opt/app/assignment/assignment.jar
ENTRYPOINT ["java", "-jar", "assignment.jar"]